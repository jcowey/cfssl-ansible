Role Name
=========

Install go and cfssl
Management for cfssl

Requirements
------------

The install phase will install dependancies including `development tools` from yum, git and go at version 1.11

Might be a good idea to have a reverse proxy infront of the service domains 

Role Variables
--------------

```yaml
---
country_name: GB
state_name: England
locality_name: London
org_name: myorg
org_unit_name: www
common_name: YOURDOMAIN.com
root_common_name: SUPER ROOT CA YAY
cert_hosts:
  - YOURDOMAIN.com
  - ifyouwantasanhostusemultiple.com
  - 2ndhost.ifyouwantasanhostusemultiple.com
expiry_time_root: 87600h
expiry_time_int: 43800h
intermediate_name: SUPER INTERMEDIATE CA YAH
cfssl_domain: cfssl.yourinteraldomain.com
ocsp_port: 8889
cfssl_port: 8888
max_path_len_intermediate: 1
```

The certs will be signed by the intermediate. Just change the `cert_hosts` and `common_name` for a new bunch of certs. 
Change the `intermediate_name` if you want these certs off a new intermediate 
This will also create ocsp for the intermediate

The directory structure is in `/etc/ssl/cfssl/ca`

Set you max path length as an interger in the vars `max_path_len_intermediate`

Note: only the root user has access to the cfssl command as it will install inside /root/go/bin
NOTE: please ensure you are changing the ocsp port and cfssl port to ensure you are configuring the correct ca

Example Playbook
----------------

Edit variables inside vars/main.yml

```
- hosts: servers
  roles:
    - cfssl
  environment:
    PATH: /root/go/bin:/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/go/bin
```

License
-------

BSD
